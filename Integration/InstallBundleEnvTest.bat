@echo off

@SET TGVFolder=%1
@SET TGVFolderToCopy=%2
@SET BundleFolderToCopy=%3
@SET BundleFolder=%4
@SET ConfigFile=%5

@echo BEGIN STEP INSTALLATION THE CONFIGURATION FILE ON TEST ENV
del %TGVFolder%\Prevoyance.TGV
del %TGVFolder%\WydePolicyAdminSolution.TGV
robocopy %TGVFolderToCopy%  %TGVFolder%    *.TGV /NP /R:0

@echo ErrorLevel: %errorlevel%
@echo "%TGVFolder%\eWAM Set Env.bat"
call "%TGVFolder%\eWAM Set Env.bat"

@echo ErrorLevel: %errorlevel%
rem @set WF-Bundle=%BundleFolderToCopy%

%EnvLocation%\..\Bin\eWAM /SYSTEM /RUNASIDE /Run:xConfigDef.BatchInstallConfig /CONFIG:"%ConfigFile%"
rem %TGVFolder%\eWAM /SYSTEM /RUNASIDE /Run:xConfigDef.BatchInstallConfig %ConfigFile%\%ConfigFileName%
@echo ErrorLevel: %errorlevel%
@set errorlevel=0