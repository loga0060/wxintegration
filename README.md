# README #


# What is this repository for? #

* WxIntegration

With WxIntegration you can automate run a number of common integration tasks with the command line. The goal it to run those tasks with a CI tool such as Jenkins or Bamboo. Product teams and project team are already using this framework with Jenkins : [https://mphasiswyde.atlassian.net/wiki/display/COM/Wynsure+CI+with+Jenkins](https://mphasiswyde.atlassian.net/wiki/display/COM/Wynsure+CI+with+Jenkins)

Here is the list of action you can run from the command line:

![CLI.jpg](https://bitbucket.org/repo/py45Ly/images/1382512534-CLI.jpg)

Those steps can be enhanced on project if needed. A number of new steps will be delivered in Wynsure 5.4 (to create the test case for example)

The config is maintained in an XML, a number of examples are available into the [Integration folder](https://bitbucket.org/wynsure/wxintegration/src/1065b284a3bb38b7b3b40ebcd13504b7f25cc573/Integration/?at=master).

# How to install it ? #

    1.Download it or clone the repository
    2.Put the content into your wyde root
    3.Close eWam
    4.Run Bin/_InstallWxIntegration.bat